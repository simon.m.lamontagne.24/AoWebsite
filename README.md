# AoWebsite

Storage for AO website

## Setup

Here are the tools you will need to edit the website. Most of these items are a one time setup. You will need:

- VSCode
- Live Server extension
- GIT

### Software Installation

First, install VSCode from [here](
https://code.visualstudio.com/download). VSCode is the most widely used software to edit code. Its basically a fancy text editor. We recommend VSCode because it allows you to install extensions that can make your life easier.

When VSCode is installed, add the [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) extension.

If you need help, I wrote a manual :) [Live Server Manual](/manuals/LiveServerInstallation/Server.md)

### Account creation

Make a GitLab account and get permissions for [gitlab.com/Katsutoshii/AoWebsite](https://gitlab.com/Katsutoshii/AoWebsite) from the previous webmaster.

Once you have permissions, we need to download the website files to your computer. There are two ways of doing this. One method involves the terminal, so if you would rather not use that method, you can download the files directly from GitLab. Otherwise, run the following in your terminal:

`git clone https://gitlab.com/Katsutoshii/AoWebsite`

### Making edits
The website is under `git` version control, which is easy to manage inside of vscode.

On the left menu bar, you can view git related activites by pressing the third button

![git](assets/vscode/git_button.png)

Any changes you make will be shown on the left, as shown below.

![git](assets/vscode/git.png)

Write a message in the box and hit the check mark to commit them.

When done, click the sync button at the bottom to save your changes to GitLab.
It may ask you to log in, which is where you should use you GitLab username and password.

![push](assets/vscode/push.png)

The steps to doing your job will more or less always be the same:

[1. Making changes to the execs](/manuals/ChangingExecs/Change.md)

[2. Giving permissions to new mems](/manuals/Permissions/permission.md)

[3. Uploading your changes to the website](/manuals/UploadingToWebsite/Upload.md)

If you need ANY help, please feel free to contact the previous webmaster. It is our duty to help you feel comfortable with the role :)
