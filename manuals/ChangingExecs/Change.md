# Making Changes To The Execs Page


## When making changes to the execs page, the activity can be split into 3 parts:
### 1. Locating the section you wanna change within the code
### 2. Changing the text itself (blurb, name, etc.)
### 3. Changing the image

### Always start by opening the website folder in VSCode and opening the local server:

Open the `AOWebsite` directory in VSCode. To do this, simply go to
`File -> Open Folder... -> Highlight the folder you want to open -> Open`

We want to open a local version of the website so that we can see the changes in real time. NOTE: YOU ARE NOT ACTUALLY MAKING CHANGES TO THE ONLINE VERSION OF THE WEBSITE, THIS IS JUST A LOCAL VERSION, SO THAT YOU CAN SEE WHAT YOUR CHANGES LOOK LIKE BEFORE YOU UPLOAD THEM. To open the live server go to `View -> Command Palette...` and in the search bar, type in "open with live server" and press the option that corresponds to that. A tab in your default browser should open up with a AO website. You know it was a success if the URL does not look like the real URL.

![live-server](../../assets/vscode/live-server.png)
---

### 1. Locating the section you wanna change within the code:

To start, you need to figure out which file the text you want to change is in. The Execs page information will always be in the execs.html file. However, if there is text somewhere else you would like to change, you can simply look at the URL of the page on the website to figure out the file name:

![file-name](FileNamesCanBeFound.png)

Once you know which file, you need to figure out where in the code that text is located. Generally, HTML is easy to read, so just going through the HTML until you see the text is usually the fastest way. However, there is an alternative way for people who do not feel comfortable with this: You can copy a section of the text you wanna locate, then use CMD + F [or ctrl + F] in VSCode to find that section of the text.

### 2. Changing the text itself (blurb, name, etc.)

For example, let's find this Vice President entry. Copy a piece, I will use the "Vice" text.

![file-name](FindInPage1.png)

The use CMD + F to find it in the code

![file-name](FindInPage2.png)

Once you have located the section generally, if you want to change the blurb, you would simply change the highlighted text:

![file-name](Highlighted.png)

### 3. Changing the image

Chaning the image isn't as simple as changing the text, but it is only slightly more complicated.
0. Add the image of the exec in the images folder under People: `/images/People/...` Try to follow the naming scheme already in place as it keeps things simple. The file format of the image does not matter, but you do need to know what format it is in (png, jpg, jpeg, etc.) for step #2. If you need help figuring out what the file format is, you can google it or ask the previous webmaster.
1. Follow the previous steps to locate the section you want to change.
2. Images in HTML will always be of the format `<Img src="FILEPATH"/>`. You want to change "FILEPATH" to point to the image you want. As long as you placed the image in the same place that all the other images are, you only really need to change the end. I have highlighted the section that would change.

![file-name](ChangePicture.png)

Note: If the picture is too long, it will spill over to the next section. You will notice in the code there are many <br></br> tags after images, these are break tags. They add a space wherever you add them. You can add or remove them freely until the section comfortable encompasses the image.

# Once you have made all the changes you need to make right now, all you have to do is push those changes to github and then upload the files. [Uploading manual](/../UploadingToWebsite/Upload.md). Done :)
