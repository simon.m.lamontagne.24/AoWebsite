# Uploading the modified files to the website

If you have not already, go to the section of the manual that covers making changes. Once have made the changes you need, then you can upload the files by following this guide.

I recommend going back to the main guide and following it to upload your changes to github. [Main Manual](/../../README.md)

NOTE: When signing into the Dartmouth account in the first step, I suggest having another browser, that you don't use as your main browser, handy. This is because I do not know how to manually log out of the Alpha Theta Dartmouth account, so you might have trouble signing into your own Dartmouth account.

You need to have the netid and password of the Alpha Theta Dartmouth account ready. If you do not, contact the current President or Vice President to recive that information. Additionally, confirm with one of them that you can use their number to send the duo 2 factor authentication requests.

## 1. Log into the Alpha Theta Dartmouth account

Go to `darthub.dartmouth.edu` and type in the NetID and Password of the Alpha Theta Dartmouth Account.

You will know your sign in attempt was successful when you see this screen:

![file-name](SignIn.png)

## 2. Log into the file system

Once you have your dartmouth account set to the Alpha Theta account, you can continue to the website that hosts the Alpha Theta website.

Either in the same tab or a new one, enter the following URL: `https://host.dartmouth.edu/`

![file-name](Dashboard.png)

Once on the page, press the dashboard button on the right. I have boxed it in the image above. After scroll until you see "File manager" highlighted below:

![file-name](FileManager.png)

Once in the file manager, you can ignore everything and press the `public_html` folder. This contains all the the files for the website.

![file-name](public_html.png)

Once in the folder, the next screen should look familiar: It is the exact file structure of your local copy of the website. To upload, you simply need to press the upload button above.

![file-name](ImagesAndExecs.png)

### IMPORTANT NOTE: If you are changing a file within another folder, you MUST navigate to that folder then press upload. For example, when uploading the images of new execs, you must navigate to the images folder, then to the People folder, then press upload. Otherwise the images will not show up on the website.
